package unidad1fractales;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jesus
 */
public class Lineas extends JFrame {
        public int nivel=4;
        
    
    public static void main(String[] args) {
        new Lineas().setVisible(true);
    }
    
    public Lineas(){
        super("Lineas");
        setBounds(100, 100, 800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke( new BasicStroke( 4 ) );
    g2.setColor(Color.BLACK);
    dibujar(g2, 100,200,600);
    }
    public void dibujar(Graphics g, int x, int y, int len) {
        if(len>=1)
        {
            g.drawLine(x, y, x+len, y);
            y+=30;
            dibujar(g,x,y, len/3);
            dibujar(g,x+len*2/3,y, len/3);
            
        }
        
        }
    }
    
