package unidad1fractales;


import java.awt.Graphics;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class Koch extends JFrame
{
    
     public static void main(String[] args) {
        new Koch().setVisible(true);
    }
     
      public Koch(){
        super("Koch");
        setBounds(100, 100, 800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
     private double x=250;
    private double y=500;
    private double angulo=0;
    private double largo=500;
    private int nivel = 3;
    private int lados = 5;
    
    
    @Override
    public void paint(Graphics g) {
    g.setColor(Color.BLACK);
     g.drawString("Nivel:" + nivel, 650, 50);
    dibujarTriangulo(g);
    }
    
    

  public void dibujarKoch(Graphics g, double l, int n) {
    if (n == 0) {
      double rad = Math.toRadians(angulo);
      double x1 = x + l * Math.cos(rad);
      double y1 = y + l * Math.sin(rad);
      g.drawLine((int) x, (int) y, (int) x1, (int) y1);
      x = x1;
      y = y1;
    } else {
      l /= 3;
      n--;
      dibujarKoch(g, l, n);
      angulo += 60;
      dibujarKoch(g, l, n);
      angulo -= 120;
      dibujarKoch(g, l, n);
      angulo += 60;
      dibujarKoch(g, l, n);
    }
  }
    
    public void dibujarTriangulo(Graphics g) {
    angulo = 0;
    dibujarKoch(g, largo, nivel);
    angulo -= 120;
    dibujarKoch(g, largo, nivel);
    angulo -= 120;
    dibujarKoch(g, largo, nivel);
  }
    
    public void dibujarPoligono(Graphics g, int n){
      int l=1500/n;
      int a =360/n;
      for(int i=0;i<n;i++){
          if(i==0){
              angulo =0;
              dibujarKoch(g, l, nivel);
          }else{
              angulo-=a;
              dibujarKoch(g, l, nivel);
          }
      }
  }
}
