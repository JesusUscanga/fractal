package unidad1fractales;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
/**
 *
 * @author Jesus
 */
public class TrianguloSierpinski extends JFrame {
    
    public int nivel=5;
    
    public static void main(String[] args) {
        new TrianguloSierpinski().setVisible(true);
    }
    
    public TrianguloSierpinski(){
        super("Triangulo Sierpinski");
        setBounds(100, 100, 800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void paint(Graphics g) {
    g.setColor(Color.BLACK);
     g.drawString("Nivel:" + nivel, 650, 50);
    dibujar(g, 700, nivel, 50, 650);
    }
    public void dibujar(Graphics g, double l,
            int n, double x, double y) {
        if (n == 0) {
            int[] cX = new int[3];
            int[] cY = new int[3];
            cX[0] = (int) x;
            cY[0] = (int) y;
            cX[1] = (int) (x + l);
            cY[1] = (int) y;
            cX[2] = (int) (x + l/2);
            cY[2] = (int) (y - l*Math.sin(Math.PI/3));
            g.setColor(Color.BLACK);
            g.fillPolygon(cX,cY,3);
        } else {
            l = l/2;
            n--;
            dibujar(g,l,n,x,y);
            dibujar(g,l,n,x+l,y);
            dibujar(g,l,n,x+l/2,y-l*Math.sin(Math.PI/3));
        }
    }
}
