package unidad1fractales;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jesus
 */
public class Circulos extends JFrame {
       public int nivel=4;
    
    
    public static void main(String[] args) {
        new Circulos().setVisible(true);
    }
    
    public Circulos(){
        super("Circulos");
        setBounds(100, 100, 800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void paint(Graphics g) {
    g.setColor(Color.BLACK);
     g.drawString("Nivel:" + nivel, 650, 50);
    dibujar(g, 300, 200, 2);
    }
    public void dibujar(Graphics g, int x, int y, int radio) {
        g.drawOval(x, y, radio, radio);
        if(radio<100){
        dibujar(g,x+radio*2,y,radio*2);
        dibujar(g,x-radio*2,y,radio*2);
        
        }
    }
}
